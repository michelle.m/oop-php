<?php 

require_once ("animal.php");
require_once ("ape.php");
require_once ("frog.php");

$sheep = new Animal("shaun");
echo "nama hewan : ".$sheep->name."<br>";
echo "jumlah kaki : ".$sheep->legs."<br>";
echo "cold_blooded? : ".$sheep->cold_blooded."<br><br>";

$ape = new Ape("kera sakti");
echo "nama hewan : ".$ape->name."<br>";
echo "jumlah kaki : ".$ape->legs."<br>";
echo "cold_blooded? : ".$ape->cold_blooded."<br><br>";

$frog = new Frog("buduk");
echo "nama hewan : ".$frog->name."<br>";
echo "jumlah kaki : ".$frog->legs."<br>";
echo "cold_blooded? : ".$frog->cold_blooded."<br>";

?>